FROM ubuntu:latest

RUN apt-get update
RUN apt-get -y upgrade
# vim
RUN apt-get install -y apt-utils vim wget locales
RUN apt update && apt -y upgrade

# Set the locale
RUN apt-get install -y locales language-pack-ko
RUN locale-gen ko_KR.UTF-8
ENV LANG ko_KR.UTF-8
ENV LANGUAGE ko_KR:kr
ENV LC_ALL ko_KR.UTF-8

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 

ADD catalina.sh /tmp/catalina.sh

RUN chmod +x /tmp/catalina.sh

CMD ["/tmp/catalina.sh"]
